Creating new styles is a lot like creating drupal themes.

1. Create a new dir in the css dir. Your directory name should be uri safe (only use alpha-numeric, underscores and dashes - do not use spaces)

2. Create a .css stylesheet with the same name as your directory + the .css extension. If your directory is my_style, then your stylesheet would be named my_style.css.

3. Create a text file named description.txt and describe your stylesheet.

4. create a screen shot named screenshot.png.

5. (not implamented yet)
If your style uses images the images should be placed in an css_images folder and referenced from the stylesheet using css_images - the css_images folder will be placed in the root of the .jar file.

